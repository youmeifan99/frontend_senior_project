import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Modal,
  Image,
  PixelRatio,
} from 'react-native';
import colors from '../../Colors';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';

const DetailsScreen = ({navigation}) => {
  state = {
    profile_1: null,
    profile_2: null,
    profile_3: null,
    profile_4: null,
    videoSource: null,
    studentListVisible: false,
    studentList: null
  };

  
    // selectPhotoTapped_1 = selectPhotoTapped_1.bind(this);
    // selectPhotoTapped_2 = selectPhotoTapped_2.bind(this);
    // selectPhotoTapped_3 = selectPhotoTapped_3.bind(this);
    // selectPhotoTapped_4 = selectPhotoTapped_4.bind(this);
   

  const scanImages =()=> {
    const imagesBase64URLs = {
      image_1: this.state.profile_1,
      image_2: this.state.profile_2,
      image_3: this.state.profile_3,
      image_4: this.state.profile_4,
    };
    // console.log('===========================', imagesBase64URLs);
    // axios.post('http://127.0.0.1:5000/images', imagesBase64URLs).then((res) => {
    //   if (res) {
    //     console.log('===========================', res.data);
    //     this.setState({studentList: res.data})
    //   }
    // });
    this.setState({studentListVisible: !this.state.studentListVisible});

  }

  let selectPhotoTapped_1=()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_1: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  let selectPhotoTapped_2=()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_2: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  let selectPhotoTapped_3 =()=>{
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_3: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  let selectPhotoTapped_4 =()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_4: source,
        });
        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }
  return (
    <View style={styles.container}>

      <View style={[{flexDirection: 'row'}]}>
        <TouchableOpacity onPress={selectPhotoTapped_1.bind(this)}>
          <View
            style={[
              styles.avatar,
              styles.avatarContainer,
              {marginBottom: 20, marginLeft: 10},
            ]}>
            {this.state.profile_1 === null ? (
              <Text>Select a Photo</Text>
            ) : (
              <Image
                style={styles.avatar}
                source={this.state.profile_1}
              />
            )}
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={selectPhotoTapped_2.bind(this)}>
          <View
            style={[
              styles.avatar,
              styles.avatarContainer,
              {marginBottom: 20, marginLeft: 10, marginRight: 10},
            ]}>
            {this.state.profile_2 === null ? (
              <Text>Select a Photo</Text>
            ) : (
              <Image
                style={styles.avatar}
                source={this.state.profile_2}
              />
            )}
          </View>
        </TouchableOpacity>
      </View>

      <View style={[{flexDirection: 'row'}]}>
        <TouchableOpacity onPress={selectPhotoTapped_3.bind(this)}>
          <View
            style={[
              styles.avatar,
              styles.avatarContainer,
              {marginLeft: 10, marginBottom: 20, marginRight: 10},
            ]}>
            {this.state.profile_3 === null ? (
              <Text>Select a Photo</Text>
            ) : (
              <Image
                style={styles.avatar}
                source={this.state.profile_3}
              />
            )}
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={selectPhotoTapped_4.bind(this)}>
          <View
            style={[
              styles.avatar,
              styles.avatarContainer,
              {marginBottom: 20, marginRight: 10},
            ]}>
            {this.state.profile_4 === null ? (
              <Text>Select a Photo</Text>
            ) : (
              <Image
                style={styles.avatar}
                source={this.state.profile_4}
              />
            )}
          </View>
        </TouchableOpacity>
      </View>

      <TouchableOpacity
        style={[styles.create, {backgroundColor: 'mediumpurple'}]}
        type="submit"
        >
        <Text
          style={{
            color: colors.white,
            fontWeight: '600',
          }}>
          Submit
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default DetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 10,
    width: 200,
    height: 200,
  },
  create: {
    marginTop: 24,
    marginLeft: 20,
    marginRight: 20,
    height: 50,
    width: 200,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
});