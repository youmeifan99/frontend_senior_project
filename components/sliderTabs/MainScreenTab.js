import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainScence from '../MainScreen';
import StuFace from './StudentFace';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import {NavigationContainer} from '@react-navigation/native';

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const FaceStack = createStackNavigator();

function MainTabScreen(props) {
  console.log('=========', props.role);
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeStackScreen} />
      <Tab.Screen name="StuFace" component={StuFaceStackScreen} />
    </Tab.Navigator>
  );
}

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => (
  <HomeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={MainScence}
      options={{
        title: '',
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="#fff"
            color="mediumpurple"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </HomeStack.Navigator>
);

const StuFaceStackScreen = ({navigation}) => (
  <FaceStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'mediumpurple',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <FaceStack.Screen
      name="Stuface"
      component={StuFace}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="ios-menu"
            size={25}
            backgroundColor="mediumpurple"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </FaceStack.Navigator>
);
