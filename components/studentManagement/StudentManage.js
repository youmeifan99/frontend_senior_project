import React, {Component} from 'react';
import {
  AppRegistry,
  FlatList,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  SafeAreaView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import Swipeout from 'react-native-swipeout';
import {List, ListItem, Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import * as RNFS from 'react-native-fs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CreateNewStudent from './CreateNewStudent';

class FlatListItem extends Component {
  state = {
    activeRowKey: null,
  };

  componentDidMount() {
    console.log('Item: ', this.props.item);
  }

  deleteData() {
    axios
      .delete('http://127.0.0.1:5000/courses', {
        data: {
          isDeletingCourse: 'Student',
          courseId: this.props.index,
          stuId: this.props.item.stuId,
          firstName: this.props.item.firstName,
          lastName: this.props.item.lastName,
          stuFace: this.props.item.stuFace,
        },
      })
      .then((res) => {
        if (res) {
          console.log('Deleted');
        }
      });

    // this.props.closeModal();
  }

  render() {
    const swipeSettings = {
      autoClose: true,

      onClose: (secId, rowId, direction) => {
        if (this.state.activeRowKey != null) {
          this.setState({activeRowKey: null});
        }
      },
      onOpen: (secId, rowId, direction) => {
        this.setState({activeRowKey: this.props.item.stuId});
      },
      right: [
        {
          onPress: () => {
            const deletingRow = this.state.activeRowKey;
            Alert.alert(
              'Alert',
              'Are you sure you want to delete ?',
              [
                {
                  text: 'No',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'Yes',
                  onPress: () => {
                    this.deleteData();
                    //Refresh FlatList !
                    // this.props.parentFlatList.refreshFlatList(
                    //     deletingRow
                    // );
                  },
                },
              ],
              {cancelable: true},
            );
          },
          text: 'Delete',
          type: 'delete',
        },
      ],
      rowId: this.props.index,
      sectionId: 1,
    };

    return (
      <Swipeout {...swipeSettings}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'mediumpurple',
          }}>
          <View style={styles.flatListItem}>
            <Text style={styles.flatListItem}>{this.props.item.firstName}</Text>
            <Text style={styles.flatListItem}>{this.props.item.lastName}</Text>
            <Text style={styles.flatListItem}>{this.props.item.stuId}</Text>
          </View>

          {/* <Text style={styles.flatListItem}>{this.props.item._id.$oid}</Text> */}
        </View>
      </Swipeout>
    );
  }
}

export default class StudentManage extends React.Component {
  state = {
    data: [],
    createNewStudent: false,
    courseId: this.props.list.courseId,
    id: this.props.list._id.$oid,
  };

  componentDidMount() {
    this.fetchData();
    console.log(this.state.courseId);
  }

  fetchData = async () => {
    console.log('ID: ', this.state.id);
    const response = await fetch(
      `http://127.0.0.1:5000/course/${this.state.id}`,
    );
    const json = await response.json();
    console.log('data===', json);
    console.log(this.state.courseId);
    this.setState({data: json.students});
  };

  toggleAddTodoModal() {
    this.setState({createNewStudent: !this.state.createNewStudent});
  }

  async selectDocument(courseId) {
    try {
      const result = await DocumentPicker.pick({});
      console.log('File Path: ', result.uri.replace(/%20/g, ' '));
      console.log('cadscdsvcea', courseId);

      await RNFS.readFile(result.uri.replace(/%20/g, ' '))
        .then((res) => {
          var dataArrary = res.slice(22).split(' ');
          //   var dataArrary = res.replace(/%20/g, '');

          console.log('Data: ', dataArrary);
          // console.log('Data: ', dataArrary[10].split(','));
          var studnetList = [];
          var studnetId = [];
          var firstName = [];
          var lastName = [];
          for (i = 0; i < dataArrary.length - 1; i++) {
            studnetList.push(dataArrary[i]);
            studnetId.push(studnetList[i].split(',')[2].slice(3).slice(0, 9));
            firstName.push(studnetList[i].split(',')[4]);
            lastName.push(studnetList[i].split(',')[5]);

            const studentsList = {
              isPostingCourse: 'Student',
              courseId: courseId,
              stuId: studnetId[i],
              firstName: firstName[i],
              lastName: lastName[i],
              stuFace: '',
            };
            console.log('===========================', studentsList);
            axios
              .post('http://127.0.0.1:5000/courses', studentsList)
              .then((res) => {
                if (res) {
                  console.log('added', studentsList);
                }
              });
          }
          //   console.log('studnetList: ', studnetList[1].split(',')[2]);
          console.log('studnetId', studnetId);
          console.log('firstName', firstName);
          console.log('lastName', lastName);
        })
        .catch((err) => {
          console.log(err.message, err.code);
        });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.title}>
          <Text style={styles.title}>First Name</Text>
          <Text style={styles.title}>Last Name</Text>
          <Text style={styles.title}>Student ID</Text>
        </View>
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 10,
            right: 32,
            zIndex: 10,
          }}
          onPress={this.props.closeModal}>
          <MaterialIcons name="close" size={24} color={colors.black} />
        </TouchableOpacity>

        <Modal
          animationType="slide"
          visible={this.state.createNewStudent}
          onRequestClose={() => this.toggleAddTodoModal()}>
          <CreateNewStudent
            list={this.props.list}
            closeModal={() => this.toggleAddTodoModal()}
          />
        </Modal>

        <View style={{flex: 1, marginTop: 45}}>
          <FlatList
            data={this.state.data}
            renderItem={({item}) => {
              return (
                <FlatListItem
                  item={item}
                  index={this.state.courseId}></FlatListItem>
              );
            }}></FlatList>
        </View>
        <View style={styles.button}>
          <MaterialIcons
            reverse
            name="cloud-upload"
            size={30}
            // index={this.setState({courseId: this.props.list.courseId})}
            onPress={() =>
              this.selectDocument(this.props.list.courseId)
            }></MaterialIcons>
          <MaterialIcons
            reverse
            name="create"
            size={30}
            onPress={() => this.toggleAddTodoModal()}></MaterialIcons>
        </View>
        <View style={styles.button}>
          <Text>Upload</Text>
          <Text>Create</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 45,
    flex: 1,
  },
  title: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    color: 'black',
    padding: 5,
    fontSize: 15,
    marginTop: 20,
    marginBottom: -20,
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
  flatListItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    color: 'white',
    padding: 5,
    fontSize: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 5,
    // marginBottom: 16,
    paddingLeft: 50,
    paddingRight: 50,
  },
});
