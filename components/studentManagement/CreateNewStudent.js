import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';

export default class CreateNewStudent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fName: '',
      lName: '',
      stuId: '',
    };
  }

  createTodo = (event) => {
    const student = {
      isPostingCourse: 'Student',
      courseId: this.props.list.courseId,
      stuId: this.state.stuId,
      firstName: this.state.fName,
      lastName: this.state.lName,
      stuFace: '',
    };
    event.persist();

    // console.log('Studnet info: ', student);
    axios.post('http://127.0.0.1:5000/courses', student).then((res) => {
      if (res) {
        console.log('added', student);
      }
    });
    this.props.closeModal();
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 10,
            right: 32,
            zIndex: 10,
          }}
          onPress={this.props.closeModal}>
          <MaterialIcons name="close" size={24} color={colors.black} />
        </TouchableOpacity>
        <View>
          <Text style={styles.title}>Add Student</Text>
          <Text>Student ID:</Text>
          <TextInput
            style={styles.input}
            placeholder="Studnet ID"
            onChangeText={(text) => this.setState({stuId: text})}
            name="studentId"
            value={this.state.stuId}
          />
          <Text>First Name:</Text>
          <TextInput
            style={styles.input}
            placeholder="First Name"
            onChangeText={(text) => this.setState({fName: text})}
            name="firstName"
            value={this.state.fName}
          />
          <Text>Last Name:</Text>
          <TextInput
            style={styles.input}
            placeholder="Last Name"
            onChangeText={(text) => this.setState({lName: text})}
            name="lastName"
            value={this.state.lName}
          />

          <TouchableOpacity
            style={[styles.create, {backgroundColor: 'mediumpurple'}]}
            type="submit"
            onPress={this.createTodo}>
            <Text
              style={{
                color: colors.white,
                fontWeight: '600',
              }}>
              Add New student
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 45,
    flex: 1,
  },
  title: {
    fontSize: 28,
    fontWeight: '800',
    color: colors.black,
    alignSelf: 'center',
    marginTop: 50,
    marginBottom: 16,
  },
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.blue,
    borderRadius: 6,
    height: 50,
    marginTop: 8,
    marginRight: 10,
    marginLeft: 10,
    paddingHorizontal: 16,
    fontSize: 18,
  },
  create: {
    marginTop: 24,
    marginLeft: 20,
    marginRight: 20,
    height: 50,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  colorSelect: {
    width: 30,
    height: 30,
    borderRadius: 4,
  },
  button: {
    width: 250,
    height: 50,
    backgroundColor: '#330066',
    borderRadius: 30,
    justifyContent: 'center',
    marginBottom: 20,
  },
  text: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
});

// export default CreateNewStudent;
