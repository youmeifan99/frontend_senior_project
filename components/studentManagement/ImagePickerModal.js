import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Modal,
  Image,
  PixelRatio,
} from 'react-native';
import colors from '../../Colors';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import StudentList from './StudentList';
import { isNull } from 'lodash';
// import ImagePicker from 'react-native-customized-image-picker';

export default class ImagePickerModal extends React.Component {
  state = {
    avatarSource_1: null,
    avatarSource_2: null,
    avatarSource_3: null,
    avatarSource_4: null,
    videoSource: null,
    studentListVisible: false,
    // studentList: null,
    // fName:'',
    // lName:'',
    // stuId:''
  };

  constructor(props) {
    super(props);

    this.selectPhotoTapped_1 = this.selectPhotoTapped_1.bind(this);
    this.selectPhotoTapped_2 = this.selectPhotoTapped_2.bind(this);
    this.selectPhotoTapped_3 = this.selectPhotoTapped_3.bind(this);
    this.selectPhotoTapped_4 = this.selectPhotoTapped_4.bind(this);
  }
  // componentDidMount() {
  //   console.log('+++++++++++++++', this.props.data);
  // }

  scanImages() {
    const imagesBase64URLs = {
      image_1: this.state.avatarSource_1,
      image_2: this.state.avatarSource_2,
      image_3: this.state.avatarSource_3,
      image_4: this.state.avatarSource_4,
    };
    // console.log('===========================', imagesBase64URLs);
    axios.post('http://127.0.0.1:5000/images', imagesBase64URLs).then((res) => {
      if (res) {
        console.log('===========================res.data', res.data);
        this.setState({studentList: res.data})
      }
      // console.log('===========================', this.state.studentList);
    });
    // this.saveStudentList()
    this.setState({studentListVisible: !this.state.studentListVisible});
  }

//   saveStudentList(){
//     this.state.studentList.map((item) => {
//       Axios.get(`http://127.0.0.1:5000/student/${item}`
//     ).then((response) => {
//     console.log("=======_________",response)
//       // this.setState({
//       //   fName:response.data.firstname_EN,
//       //   lName:response.data.lastname_EN,
//       //   stuId:response.data.student_id,
//       // })

//       const studentInfo = {
//         isPostingCourse: 'AttendanceList',
//         courseId: this.props.data.courseId,
//         stuId: response.data.student_id,
//         firstName: response.data.firstname_EN,
//         lastName: response.data.lastname_EN,
//       }
//       Axios.post('http://127.0.0.1:5000/courses', studentInfo)
//       .then((res) => {
//         if (res) {
//           console.log('added', studentInfo);
//         }
//       });
//     })
//   });
// }


  // togoAttendanceList() {
  //   this.setState({studentListVisible: !this.state.studentListVisible});
  // }


  selectPhotoTapped_1() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_1: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  selectPhotoTapped_2() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_2: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  selectPhotoTapped_3() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_3: source,
        });

        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  selectPhotoTapped_4() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // let source = {uri: response.uri};

        // You can also display the image using data:
        let source = {uri: 'data:image/jpeg;base64,' + response.data};

        this.setState({
          avatarSource_4: source,
        });
        console.log('imageURL: ' + 'data:image/jpeg;base64,' + response.data);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{position: 'absolute', top: 50, right: 20, zIndex: 99}}
          onPress={this.props.closeModal}>
          <MaterialIcons name="close" size={24} color={colors.black} />
        </TouchableOpacity>

        <View style={[{flexDirection: 'row'}]}>
          <TouchableOpacity onPress={this.selectPhotoTapped_1.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginBottom: 20, marginLeft: 10},
              ]}>
              {this.state.avatarSource_1 === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image
                  style={styles.avatar}
                  source={this.state.avatarSource_1}
                />
              )}
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.selectPhotoTapped_2.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginBottom: 20, marginLeft: 10, marginRight: 10},
              ]}>
              {this.state.avatarSource_2 === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image
                  style={styles.avatar}
                  source={this.state.avatarSource_2}
                />
              )}
            </View>
          </TouchableOpacity>
        </View>

        <View style={[{flexDirection: 'row'}]}>
          <TouchableOpacity onPress={this.selectPhotoTapped_3.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginLeft: 10, marginBottom: 20, marginRight: 10},
              ]}>
              {this.state.avatarSource_3 === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image
                  style={styles.avatar}
                  source={this.state.avatarSource_3}
                />
              )}
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.selectPhotoTapped_4.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                {marginBottom: 20, marginRight: 10},
              ]}>
              {this.state.avatarSource_4 === null ? (
                <Text>Select a Photo</Text>
              ) : (
                <Image
                  style={styles.avatar}
                  source={this.state.avatarSource_4}
                />
              )}
            </View>
          </TouchableOpacity>
          
        </View>
        <Modal
          animationType="slide"
          visible={this.state.studentListVisible}
          onRequestClose={() => this.scanImages()}>
          <StudentList
            list={this.state.studentList}
            courseId={this.props.courseId}
            // data={this.props}
            closeModal={() => this.scanImages()}
          />
        </Modal>

        <TouchableOpacity
          style={[styles.create, {backgroundColor: 'mediumpurple'}]}
          type="submit"
          onPress={() => this.scanImages()}>
          <Text
            style={{
              color: colors.white,
              fontWeight: '600',
            }}>
            Scan photo
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={[styles.create, {backgroundColor: 'mediumpurple'}]}
          type="submit"
          onPress={() => this.togoAttendanceList()}>
          <Text
            style={{
              color: colors.white,
              fontWeight: '600',
            }}>
            View Attendance List
          </Text>
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 10,
    width: 200,
    height: 200,
  },
  create: {
    marginTop: 24,
    marginLeft: 20,
    marginRight: 20,
    height: 50,
    width: 350,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
