import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Modal,
  Image,
  PixelRatio,
  FlatList
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Swipeout from 'react-native-swipeout';
import Axios from 'axios';
import _ from 'lodash';

export default class StudentList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      studentList: []
    }
  }
  componentDidMount() {
    const {list} = this.props;
    //const {item} = list;
    console.log('+++++++++++++++ item', list);
    console.warn('list', list)
    // console.log('(((((((((((((+++++++++++++++)))))))))))))', this.props.data);
    Axios.post('http://127.0.0.1:5000/student/check-attendance',{courseId: '953005', studentList:this.props.list }
    ).then((response) => {
        console.log('response checking attendance:', response)
    }).catch(error=> {
      console.warn('checking attendance:', error);
    })
  }

 
  render() {
    console.log('student list:', this.props.list)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.title}>
          <Text style={styles.title}>First Name</Text>
          <Text style={styles.title}>Last Name</Text>
          <Text style={styles.title}>Student ID</Text>
        </View>
        <View style={{flex: 1, marginTop: 45}}>
          <FlatList
            data={this.props.list}
            renderItem={({item}) => {
              return (
                <FlatListItem
                  item={item}
                  ></FlatListItem>
              );
            }}></FlatList>
        </View>
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 50,
            right: 32,
            zIndex: 10,
          }}
          onPress={this.props.closeModal}>
          <MaterialIcons name="close" size={24} color={colors.black} />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

class FlatListItem extends Component {
 state ={
   fName:'',
   lName:'',
   stuId:''
 }

  componentDidMount() {
    console.log('Item: ', this.props.item);
    role_id = this.props.item
    Axios.get(`http://127.0.0.1:5000/student/${this.props.item}`
    ).then((response) => {
    console.log("=======_________",response)
      this.setState({
        fName:response.data.firstname_EN,
        lName:response.data.lastname_EN,
        stuId:response.data.student_id,
      })
    })
  }

  // getAttendanList() {
  //   this.
  // }

  render() {
    return (
      <Swipeout>
        <View
          style={{
            flex: 1,
            backgroundColor: 'mediumpurple',
          }}>
          <View style={styles.flatListItem}>
            <Text style={styles.flatListItem}>{this.state.fName}</Text>
            <Text style={styles.flatListItem}>{this.state.lName}</Text> 
            <Text style={styles.flatListItem}>{this.state.stuId}</Text>
          </View>

          {/* <Text style={styles.flatListItem}>{this.props.item._id.$oid}</Text> */}
        </View>
      </Swipeout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  title: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    color: 'black',
    padding: 5,
    fontSize: 15,
    marginTop: 20,
    marginBottom: -20,
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
  flatListItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    color: 'white',
    padding: 5,
    fontSize: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 5,
    // marginBottom: 16,
    paddingLeft: 50,
    paddingRight: 50,
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 10,
    width: 200,
    height: 200,
  },
  create: {
    marginTop: 24,
    marginLeft: 20,
    marginRight: 20,
    height: 50,
    width: 200,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatListItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    color: 'white',
    padding: 5,
    fontSize: 15,
    marginTop: 5,
    marginBottom: 5,
  },
});
