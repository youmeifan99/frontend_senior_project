import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import Axios from 'axios';
import _ from 'lodash';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { log } from 'react-native-reanimated';

export default class ITSC extends Component {
  constructor(props) {
    super(props)
    this.state = {
      code: '',
      access_token: '',
      info: []
    }
  }
  webview = null;
  componentDidMount() {
    fetch('http://127.0.0.1:5000/student')
    .then((res) => res.json())
    .then((json) => {
      this.setState({
        info: json.map((item)=>{
          return {id:item.student_id}
        }),
      });
      console.log("###########",this.state.info[0].id);
    });
  }

  _onNavigationStateChange(webViewState) {
    var code = webViewState.url.replace('http://localhost:8081/?code=', '');
    console.log('just print', code);
    this.setState({ code: code })
    //this.getToken(code)
    !_.startsWith(code, 'https://oauth.cmu.ac.th/v1/Authorize.aspx?') ? this.getToken(code) : null;






    // this.props.closeModal();
  }

  getToken(code) {
    // const data = FormData({
    //   'fromObject': {
    //     code,
    //     redirect_uri:'http://localhost:8081',
    //     client_id:'KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8',
    //     client_secret: 'ZbwY9Ap5aejdD5qKmnRKa0cdhAU2USnr8Cb3RWNT',
    //     grant_type: 'authorization_code'
    //   }
    // });
    //const {code} = this.state;

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST'
    };

    const data = {
      code,
      redirect_uri: 'http://localhost:8081',
      client_id: 'KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8',
      client_secret: 'ZbwY9Ap5aejdD5qKmnRKa0cdhAU2USnr8Cb3RWNT',
      grant_type: 'authorization_code',

    }

    // console.log('formData====', data)


    Axios.post(
      'https://oauth.cmu.ac.th/v1/GetToken.aspx', null, { headers: headers, params: data })
      .then((response) => {
        console.log("access_token", response.data.access_token);
        this.setState({ access_token: response.data.access_token })
        this._getUserInfo(response.data.access_token)
      })
      .catch((error) => {
        console.warn('error', error);
      });
  }

  _getUserInfo(access_token) {

    const headers = {
      'Authorization': `Bearer ${access_token}`,
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',

    }
    console.log('getUserInfo access_token', access_token)
    Axios.get('https://misapi.cmu.ac.th/cmuitaccount/v1/api/cmuitaccount/basicinfo', { headers: headers }).then((response) => {
      console.log('user info MUST print:', response.data)
      this.storeStudentInfo(response.data)
      console.log("csdvdfsgfdsbgadfbafghfeadbad",response.data)
      Actions.replace('nav',response.data)
    }).catch(error => {
      console.log('error from _getUserInfo', error)
    })
  }

  storeStudentInfo(data) {
    // console.log("student info========", data)
    
      for(i=0;i<this.state.info.length;i++){
        if (data.student_id!==this.state.info[i].id){
          if (data.itaccounttype_id == "StdAcc") {
            const studentInfo = {
              account: data.cmuitaccount,
              student_id: data.student_id,
              firstname_EN: data.firstname_EN,
              lastname_EN: data.lastname_EN,
              organization: data.organization_name_EN,
              role_id: data.itaccounttype_id
            }
            Axios.post('http://127.0.0.1:5000/student', studentInfo).then((res) => {
              if (res) {
                console.log('added', studentInfo);
              }
            });
          } else if (data.itaccounttype_id == "MISEmpAcc") {
            const teacherInfo = {
              account: data.cmuitaccount,
              firstname_EN: data.firstname_EN,
              lastname_EN: data.lastname_EN,
              role_id: data.itaccounttype_id
            }
            Axios.post('http://127.0.0.1:5000/teacher', teacherInfo).then((res) => {
              if (res) {
                console.log('added', techerInfo);
              }
            });
          }
        }else{
          console.log("@@@@@@@@@@@@@@@@@@");
        }
      }
  }

  render() {
    //this.getToken('gVP07MVa3wxcwg678qcshJJQVPZxxnAc')
    return (
      <WebView
        ref="webview"
        source={{
          uri:
            'https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8&redirect_uri=http://localhost:8081&scope=cmuitaccount.basicinfo',
        }}
        onNavigationStateChange={this._onNavigationStateChange.bind(this)}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        // injectedJavaScript={this.state.cookie}
        startInLoadingState={false}
      />
    );
  }
}
