import React, {Component} from 'react';
import {
  View,
  Text,
  Button,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  Linking,
  Modal,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SignInScene from '../landingPage/SignInScene';

class AuthScene extends Component {
  state = {
    signInPage: false,
    // url:
    //   'https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8&redirect_uri=http://localhost:8081/callback&scope=cmuitaccount.basicinfo&state=xyz',
  };

  toggleSginInModal() {
    this.setState({signInPage: !this.state.signInPage});
  }

  login = () => {
    Linking.canOpenURL(this.state.url).then((supported) => {
      if (supported) {
        Linking.openURL(this.state.url);
      } else {
        console.log('Can not open');
      }
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Animatable.Image
            animation="bounceIn"
            duraton="1500"
            source={require('../../assets/images/logo.png')}
            style={styles.logo}
            resizeMode="stretch"
          />
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.title}>
            School Attendance Assistant with Face Recognition
          </Text>
          <Text style={styles.text}>
            Check class attendance in an easy way :)
          </Text>
          <View style={styles.button}>
            <TouchableOpacity onPress={() => this.toggleSginInModal()}>
              <LinearGradient
                colors={['#08d4c4', 'mediumpurple']}
                style={styles.signIn}>
                <Text style={styles.textSign}>Go</Text>
                <MaterialIcons name="navigate-next" color="#fff" size={20} />
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Animatable.View>
        <Modal
          animationType="slide"
          visible={this.state.signInPage}
          onRequestClose={() => this.toggleAddTodoModal()}>
          <SignInScene
            list={this.props.list}
            closeModal={() => this.toggleAddTodoModal()}
          />
        </Modal>
      </View>
    );
  }
}

export default AuthScene;

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'mediumpurple',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold',
  },
});
