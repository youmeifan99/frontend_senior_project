import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Animated,
  TextInput,
  TouchableOpacity,
  Linking,
  Modal,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import {color} from 'react-native-reanimated';
import * as Animatable from 'react-native-animatable';
import {Actions} from 'react-native-router-flux';
import {parseZone} from 'moment';
import Axios from 'axios';
import ITSC from './ITSCpage';

export default class SignInScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isITSC: false,
      check_textInputChange: false,
      password: '',
      secureTextEntry: true,
      role: '',
      url:
        // 'https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8&redirect_uri=http://localhost:8081&scope=cmuitaccount.basicinfo&state=xyz',
        'https://oauth.cmu.ac.th/v1/Authorize.aspx?response_type=code&client_id=KVZYykNWNBd2VSa0tPA05aHMgZjXUHtQj28vPcR8&redirect_uri=http://localhost:8081&scope=cmuitaccount.basicinfo&state=xyz.aspx',
    };
  }
  toggleITSC = () => {
    this.setState({isITSC: !this.state.isITSC});

    // Linking.canOpenURL(this.state.url).then((supported) => {
    //   if (supported) {
    //     Linking.openURL(this.state.url);
    //   } else {
    //     console.log('Can not open');
    //   }
    // });
  };

  // _authorizeWithCMUAccount = () => {
  //   Axios.post(this.state.url)
  //     .then((response) => {
  //       console.warn(response);
  //     })
  //     .catch((error) => {
  //       console.warn(error);
  //     });
  // };

  switchtoMain = () => {
    console.log('______________', this.state.role);

    Actions.replace('mainScreen', {role: this.state.role});
    // Actions.popTo
    // Actions.replace('nav');
  };

  textInputChange(value) {
    if (value.length !== 0) {
      this.setState({
        check_textInputChange: true,
        role: value,
      });
    } else {
      this.setState({
        check_textInputChange: false,
      });
    }
  }
  secureTextEntry() {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.text_header}>Welcome STAR !</Text>
        </View>
        <Animatable.View animation="fadeInUpBig" style={styles.footer}>
          <Text style={styles.text_footer}>E-MAIL</Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="mediumpurple" size={20} />
            <TextInput
              placeholder="Your email..."
              style={styles.textInput}
              onChangeText={(text) => this.textInputChange(text)}
            />
            {this.state.check_textInputChange ? (
              <Feather name="check-circle" color="green" size={20} />
            ) : null}
          </View>

          <Text style={(styles.text_footer, {marginTop: 35})}>Password</Text>
          <View style={styles.action}>
            <FontAwesome name="lock" color="mediumpurple" size={20} />
            {this.state.secureTextEntry ? (
              <TextInput
                placeholder="Your password..."
                secureTextEntry={true}
                style={styles.textInput}
                value={this.state.password}
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                  })
                }
              />
            ) : (
              <TextInput
                placeholder="Your password..."
                style={styles.textInput}
                value={this.state.password}
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                  })
                }
              />
            )}

            <TouchableOpacity onPress={() => this.secureTextEntry()}>
              {this.state.secureTextEntry ? (
                <Feather name="eye-off" color="gray" size={20} />
              ) : (
                <Feather name="eye" color="gray" size={20} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.signInContainer}>
            <TouchableOpacity onPress={this.switchtoMain} list={this.props}>
              <LinearGradient
                colors={['#08d4c4', 'mediumpurple']}
                style={styles.signIn}>
                <Text style={(styles.textSignIn, {color: 'white'})}>
                  Sign In
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.textOr}> ━ or ━ </Text>
          </View>
          <View>
            <TouchableOpacity
              style={styles.FacebookStyle}
              activeOpacity={0.5}
              onPress={this.toggleITSC}>
              <Image
                source={require('../../assets/images/CMU.png')}
                style={styles.ImageIconStyle}
              />
              <View style={styles.SeparatorLine} />
              <View style={styles.textContainer}>
                <Text style={styles.TextStyle}> Login Using CMU Account </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Animatable.View>
        <Modal
          animationType="slide"
          visible={this.state.isITSC}
          onRequestClose={() => this.toggleITSC()}>
          <ITSC closeModal={() => this.toggleITSC()} />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'mediumpurple',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 2,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 50,
  },
  text_header: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 10,
  },
  textInput: {
    flex: 1,
    paddingLeft: 10,
    color: 'mediumpurple',
  },
  signInContainer: {
    marginTop: 30,
    marginBottom: 10,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textContainer: {
    width: '75%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSignIn: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  FacebookStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'mediumpurple',
    // borderWidth: 0.5,
    // borderColor: '#fff',
    height: 50,
    width: '100%',
    borderRadius: 10,
    marginTop: 10,
  },
  ImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 40,
    width: 40,
    resizeMode: 'stretch',
  },
  TextStyle: {
    color: '#fff',
  },
  SeparatorLine: {
    backgroundColor: '#fff',
    width: 1,
    height: 40,
  },
  textOr: {
    textAlign: 'center',
    color: 'gray',
  },
});
