import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, Animated} from 'react-native';
import Logo from '../../assets/images/logo.png';
import {Actions} from 'react-native-router-flux';

const switchtoAuth = () => {
  Actions.replace('auth');
};

class LoadingScene extends Component {
  state = {
    LogoAnime: new Animated.Value(0),
    LogoText: new Animated.Value(0),
    loadingSpinner: false,
  };
  componentDidMount() {
    const {LogoAnime, LogoText} = this.state;
    Animated.parallel([
      Animated.spring(LogoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 3000,
        // useNativeDriver: true,
      }).start(),

      Animated.timing(LogoText, {
        toValue: 1,
        duration: 1000,
        // useNativeDriver: true,
      }),
    ]).start(() => {
      this.setState({
        loadingSpinner: true,
      });

      setTimeout(switchtoAuth, 1200);
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={{
            opacity: this.state.LogoAnime,
            top: this.state.LogoAnime.interpolate({
              inputRange: [0, 1],
              outputRange: [80, 0],
            }),
          }}>
          <Image style={styles.image} source={Logo} />
        </Animated.View>
        <Animated.View
          style={{
            opacity: this.state.LogoText,
          }}>
          <Text style={styles.logoText}>STAR</Text>
        </Animated.View>
      </View>
    );
  }
}

export default LoadingScene;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'mediumpurple',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  logoText: {
    fontFamily: 'Chalkduster',
    fontSize: 30,
    marginTop: 29.1,
    fontWeight: '700',
    color: '#FFFFFF',
  },
});
