import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
  Modal,
  PixelRatio,
} from 'react-native';
import colors from '../../Colors';
import AddListModal from './AddListModal';
import StudentManage from '../studentManagement/StudentManage';
import EditModal from './EditModal';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import ImagePickerModal from '../studentManagement/ImagePickerModal';

export default class TodoModal extends React.Component {
  state = {
    courseId: this.props.list.courseId,
    name: this.props.list.name,
    color: this.props.list.color,
    todos: this.props.list.todos,
    startTime: this.props.list.startTime,
    endTime: this.props.list.endTime,
    location: this.props.list.location,
    id: this.props.list._id,
    day: this.props.list.day,
    addTodoVisible: false,
    studentListVisible: false,
    updateListVisible: false,
    cameraModalVisible: false,
    show: false,
  };

  componentDidMount() {
    // console.log('+++++++++++++++', this.props);
    if (this.props.role == 'Student') {
      this.setState({show: false});
    } else if (this.props.role == 'Admin') {
      this.setState({show: true});
    }
  }

  toggleAddTodoModal() {
    this.setState({addTodoVisible: !this.state.addTodoVisible});
  }
  toggleStudentListVisible() {
    this.setState({studentListVisible: !this.state.studentListVisible});
  }
  toggleUpdateListVisible() {
    this.setState({updateListVisible: !this.state.updateListVisible});
  }
  toggleCameraModalVisible() {
    this.setState({cameraModalVisible: !this.state.cameraModalVisible});
  }

  deleteCourse() {
    axios
      .delete('http://127.0.0.1:5000/courses', {
        data: {isDeletingCourse: 'Course', courseId: this.state.courseId},
      })
      .then((res) => {
        if (res) {
          console.log('deleted', course);
        }
      });

    this.props.closeModal();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 64,
            right: 32,
            zIndex: 10,
          }}
          onPress={this.props.closeModal}>
          <MaterialIcons name="close" size={24} color={colors.black} />
        </TouchableOpacity>
        <View
          style={[
            styles.section,
            styles.header,
            {borderBottomColor: this.state.color},
          ]}>
          <View>
            <Text style={styles.title}>{this.state.name}</Text>
          </View>
        </View>
        <View style={[styles.section, {flex: 3}]}>
          <Text style={styles.todo}>Start Time: {this.state.startTime}</Text>
          <Text style={styles.todo}>End Time: {this.state.endTime}</Text>
          <Text style={styles.todo}>Date: {this.state.day}</Text>
          <Text style={styles.todo}>Location: {this.state.location}</Text>
        </View>
        <View style={styles.row}>
          <Modal
            animationType="slide"
            visible={this.state.addTodoVisible}
            onRequestClose={() => this.toggleAddTodoModal()}>
            <AddListModal closeModal={() => this.toggleAddTodoModal()} />
          </Modal>
          <Modal
            animationType="slide"
            visible={this.state.studentListVisible}
            onRequestClose={() => this.toggleStudentListVisible()}>
            <StudentManage
              list={this.props.list}
              closeModal={() => this.toggleStudentListVisible()}
            />
          </Modal>
          <Modal
            animationType="slide"
            visible={this.state.updateListVisible}
            onRequestClose={() => this.toggleUpdateListVisible()}>
            <EditModal
              list={this.props.list}
              closeModal={() => this.toggleUpdateListVisible()}
            />
          </Modal>

          <Modal
            animationType="slide"
            visible={this.state.cameraModalVisible}
            onRequestClose={() => this.toggleCameraModalVisible()}>
            <ImagePickerModal
             data={this.props}
             courseId = {this.state.courseId}
             closeModal={() => this.toggleCameraModalVisible()}
            />
          </Modal>
          {/* use modal to make page can redirect */}
          <MaterialIcons
            reverse
            name="edit"
            size={30}
            onPress={() => this.toggleUpdateListVisible()}
          />
          <MaterialIcons
            reverse
            name="perm-contact-calendar"
            size={30}
            onPress={() => this.toggleStudentListVisible()}
          />
          <MaterialIcons
            reverse
            name="camera"
            size={30}
            onPress={() => this.toggleCameraModalVisible()}
          />
          <MaterialIcons
            reverse
            name="delete-forever"
            size={30}
            color="#CA300E"
            onPress={() =>
              Alert.alert(
                'Delete?',
                'Cannot be undone',
                [
                  {text: 'Cancel'},
                  {
                    text: 'OK',
                    onPress: () => {
                      this.deleteCourse();
                    },
                  },
                ],
                {cancelable: false},
              )
            }
          />
        </View>
        
        <View style={styles.row}>
          <Text> Edit</Text>
          <Text> List</Text>
          <Text> Scan</Text>
          <Text> Delete</Text>
        </View>
        
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  section: {
    flex: 1,
    alignSelf: 'stretch',
  },
  header: {
    justifyContent: 'flex-end',
    marginLeft: 64,
    borderBottomWidth: 3,
  },
  title: {
    fontSize: 30,
    fontWeight: '800',
    color: colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: colors.gray,
    fontWeight: '600',
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: 'row',
    alignItems: 'center',
  },
  todo: {
    marginLeft: 65,
    color: colors.blue,
    fontWeight: '700',
    fontSize: 20,
    paddingVertical: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: -15,
    marginBottom: 16,
    paddingLeft: 30,
    paddingRight: 30,
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    // borderRadius: 75,
    width: 200,
    height: 200,
  },
});
