import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Modal,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import colors from '../Colors';
import AddListModal from './courseManagement/AddListModal';
import {List, ListItem, Input, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import TodoList from './courseManagement/TodoList';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export default class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addTodoVisible: false,
      lists: [],
      row: '',
      cols: '',
      updated: false,
    };
  }

  componentDidMount() {
    console.log('=========', this.props);
    fetch('http://127.0.0.1:5000/courses')
      .then((res) => res.json())
      .then((json) => {
        // console.log('=========', json[0].students[1].stuId);
        this.setState({
          lists: json,
        });
      });
  }

  toggleAddTodoModal() {
    // Actions.push('todo', {role: this.props.role});
    this.setState({addTodoVisible: !this.state.addTodoVisible});
  }

  renderList = (list, role) => {
    return (
      <TodoList
        list={list}
        role={this.props.role}
        updateList={this.updateList}
      />
    );
  };

  updateList = (list) => {
    this.setState({
      lists: this.state.lists.map((item) => {
        return item.id === list.id ? list : item;
      }),
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          visible={this.state.addTodoVisible}
          role={this.props.role}
          onRequestClose={() => this.toggleAddTodoModal()}>
          <AddListModal closeModal={() => this.toggleAddTodoModal()} />
        </Modal>

        <View style={{flexDirection: 'row'}}>
          <View style={styles.divider} />
          <Text style={styles.title}>
            Manage{' '}
            <Text style={{fontWeight: '300', color: colors.blue}}>Course</Text>
          </Text>
          <View style={styles.divider} />
        </View>

        <View style={{marginVertical: 48}}>
          <TouchableOpacity
            style={styles.addList}
            onPress={() => this.toggleAddTodoModal()}>
            <Icon name="plus" size={16} color={colors.blue} />
          </TouchableOpacity>

          <Text style={styles.add}>Add Course</Text>
        </View>

        <View style={{height: 300}}>
          <FlatList
            data={this.state.lists}
            keyExtractor={(item) => item.name}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => this.renderList(item, this.props.role)}
            keyboardShouldPersistTaps="always"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    backgroundColor: colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: '800',
    color: colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    borderWidth: 2,
    borderColor: colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  add: {
    color: colors.blue,
    fontWeight: '600',
    fontSize: 14,
    marginTop: 8,
  },
});
