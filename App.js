import React, {useState, useEffect, Component} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
// import MainScreen from './components/MainScreen';
import {Modal, Router, Scene} from 'react-native-router-flux';
import LoadingScene from './components/landingPage/LoadingScene';
import AuthScene from './components/landingPage/AuthScene';
import MainScence from './components/MainScreen';
import TodoModal from '../frontendApp/components/courseManagement/TodoModal';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {DrawerContent} from './components/sliderTabs/drawContent';
import MainTab from './components/sliderTabs/MainScreenTab';
import MainScreen from './components/MainScreen';


const Drawer = createDrawerNavigator();

export default class App extends Component {
  render() {
    return (
      // <View style={styles.container}>
      //   <MainScreen />
      // </View>

      <Router>
        <Scene key="root">
          <Scene
            key="loading"
            component={LoadingScene}
            initial={false}
            hideNavBar={true}></Scene>
          <Scene
            key="auth"
            component={AuthScene}
            initial={true}
            hideNavBar={true}></Scene>
          <Scene
            key="main"
            component={MainTab}
            initial={false}
            hideNavBar={true}></Scene>
          <Scene
            key="nav"
            component={Nav}
            initial={false}
            hideNavBar={true}></Scene>
          <Scene
            key="mainScreen"
            component={MainScreen}
            initial={false}
            hideNavBar={true}></Scene>
        </Scene>
      </Router>
    );
  }
}

const Nav = (props) => {
  console.log('@@@@@@@@@@@@@@@@@@', props);
  roleInfo = props;
  // props.navigation.navigate('StuFace');
  // props.navigation.push('Home', {role: props.role});
  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContent={(props) => (
          <DrawerContent {...roleInfo} {...props} />
        )}
        mainScence={(props) => (
          <MainScence {...roleInfo} {...props}/>
        )}
        >
        <Drawer.Screen name="HomeDrawer" component={MainTab} />
        {/* <Drawer.Screen name="stuFace" component={DrawerContent} /> */}
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
